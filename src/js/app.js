'use strict';

import './scroll/main';

import './scroll/menu';

import './form/validation';

import 'bootstrap';

import '../sass/styles.scss';

function importAll(r) {
    return r.keys().map(r);
}

const images = importAll(require.context('../img/', true, /\.(png|jpe?g|svg)$/));
