var menu = document.querySelector('.page-header');
var screen = document.querySelector('.screen--first');

var screenBottom = screen.getBoundingClientRect().bottom + window.pageYOffset;

window.onscroll = function () {
    if (menu.classList.contains('page-header--dark') && window.pageYOffset < screenBottom) {
        menu.classList.remove('page-header--dark');
    } else if (window.pageYOffset > screenBottom) {
        menu.classList.add('page-header--dark');
    }
};